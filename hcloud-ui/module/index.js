/** EasyWeb iframe v3.0.5 data:2018-10-24 */
String.prototype.startWith = function(str) {
    if (str == null || str == "" || this.length == 0
        || str.length > this.length)
        return false;
    if (this.substr(0, str.length) == str)
        return true;
    else
        return false;
    return true;
}

function initIFrame() {
    // var $parent = $(".admin-iframe").parent();
    // if ($parent.hasClass('layui-body')) {
    //     $parent.addClass('admin-iframe-body');
    //     return;
    // }
    // if ($parent.hasClass('layui-tab-item')) {
    //     $parent.css({'padding': '0', 'overflow-y': 'hidden'});
    // } else {
    //     // $parent.css({'width': '100%', 'height': '100%'});
    // }
}
layui.define(['layer', 'admin', 'laytpl','element','config'], function (exports) {
    var $ = layui.jquery;
    var layer = layui.layer;
    var laytpl = layui.laytpl;
    var admin = layui.admin;
    var element = layui.element;
    var config = layui.config;

    var index = {
        cacheTab: false,  // 是否记忆打开的选项卡

        initLeftNav: function () {
            Q.reg('home', function () {
                var menuPath = 'welcome.html'
                index.loadView('home', menuPath, '主页');
            });
            if(config.getMenus() && config.getAuthority()){
                var menus = config.getMenus();
                laytpl(sideNav.innerHTML).render(menus, function (html) {
                    $('*[lay-filter=admin-side-nav]').html(html);
                    element.render('nav');
                    admin.activeNav(Q.lash);
                });
                index.initRouter();
                return;
            }
            admin.reqBackground('system/authority/tree', {isMenu:1,sortProperties:'orderNum',direction:'asc'}, function (data) {
                var menus = data;
                admin.reqBackground("system/authority/getMyAuthorities",{},function(data){
                    config.putAuthority(data);
                    // 判断权限
                    for (var i = menus.length - 1; i >= 0; i--) {
                        var tempMenu = menus[i];
                        if (tempMenu.authority && !admin.hasPermission(tempMenu.authority)) {
                            menus.splice(i, 1);
                            continue;
                        }
                        if (!tempMenu.children) {
                            continue;
                        }
                        for (var j = tempMenu.children.length - 1; j >= 0; j--) {
                            var jMenus = tempMenu.children[j];
                            if (jMenus.authority && !admin.hasPermission(jMenus.authority)) {
                                tempMenu.children.splice(j, 1);
                                continue;
                            }
                            if (!jMenus.children) {
                                continue;
                            }
                            for (var k = jMenus.children.length - 1; k >= 0; k--) {
                                if (jMenus.children[k].authority && !admin.hasPermission(jMenus.children[k].authority)) {
                                    jMenus.children.splice(k, 1);
                                    continue;
                                }
                            }
                        }
                        if(!tempMenu.children.length){
                            menus.splice(i, 1);
                        }
                    }
                    config.putMenus(menus);
                    laytpl(sideNav.innerHTML).render(menus, function (html) {
                        $('*[lay-filter=admin-side-nav]').html(html);
                        element.render('nav');
                        admin.activeNav(Q.lash);
                    });
                    index.initRouter();
                },"get")

            });
        },
        initRouter: function () {
            index.regRouter(config.getMenus());
            Q.init({
                index: 'home'
            });
        },
        regRouter: function (menus) {
            $.each(menus, function (i, data) {
                if (data.menuHref && data.menuHref.indexOf('#!') == 0) {
                    Q.reg(data.menuHref.substring(2), function () {
                        var menuId = data.menuHref.substring(2);
                        var menuPath = data.menuUrl;
                        index.loadView(menuId, menuPath, data.name);
                    });

                }
                if (data.children) {
                    index.regRouter(data.children);
                }
            });
        },
        // 加载主体部分
        loadView: function (menuId,menuPath,menuName) {
            // 判断选项卡是否已添加
            var flag = false;
            $('.layui-layout-admin .layui-body .layui-tab .layui-tab-title>li').each(function () {
                if ($(this).attr('lay-id') === menuId) {
                    flag = true;
                    return false;
                }
            });
            if (!flag) {
                element.tabAdd('admin-pagetabs', {
                    title: menuName,
                    // content:  menuPath.startWith("http://") ? '<div class="admin-iframe" style="width: 100%; height: 100%;"><iframe id="' + menuId + '" src="' + menuPath + '" marginwidth="0"  marginheight="0" width="100%" height="100%"  frameborder="0" onload="initIFrame()"></iframe> </div>'   :  '<div id="' + menuId + '" class="view-card"></div>' ,
                    content: '<div id="' + menuId + '" class="view-card height100">' +
                        '          <div class="layui-fluid height100 frame-fluid">' +
                        '            <div class="layui-card height100">' +
                        '               <div class="layui-card-body height100 frame-body">   ' +
                        '               </div>' +
                        '           </div>' +
                        '       </div>' +
                        '     </div>' ,
                    id: menuId
                });
            }
            var contentDom = '#' + menuId;
            element.tabChange('admin-pagetabs', menuId);
            admin.rollPage('auto');
            if (!flag || admin.isRefresh || menuId == 'home') {
                admin.isRefresh = false;
                if(menuPath.startWith("http")){
                    $(contentDom).find(".frame-body").html('<div class="admin-iframe" ><iframe id="' + menuId + '" src="' + menuPath + '" marginwidth="0"  marginheight="0" width="100%" height="100%"  frameborder="0" onload="initIFrame()"></iframe> </div>');
                }else{
                    $(contentDom).find(".frame-body").load(menuPath, function () {
                        admin.handleViewPermission(contentDom);
                        var find = $(contentDom).find(".remove-frame-card");
                        if(find && find.length){
                            $(contentDom).find(".frame-fluid").html($(contentDom).find(".frame-body").html())
                        }
                    });
                }
            }
            admin.activeNav(Q.lash);
            // 移动设备切换页面隐藏侧导航
            if (document.body.clientWidth <= 750) {
                admin.flexible(true);
            }
        },
        // 关闭选项卡
        closeTab: function (url) {
            element.tabDelete('admin-pagetabs', url);
        },
        // 关闭选项卡记忆功能
        closeTabCache: function () {
            index.cacheTab = false;
            admin.putTempData('indexTabs', undefined);
        }
    };

    // tab选项卡切换监听
    element.on('tab(admin-pagetabs)', function (data,a,c) {
        var layId = $(this).attr('lay-id');
        Q.go(layId);
    });


    exports('index', index);
});
