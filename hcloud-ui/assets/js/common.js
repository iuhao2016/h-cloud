// 以下代码是配置layui扩展模块的目录，以及加载主题
layui.config({
    base: './module/'
}).extend({
    formSelects: 'formSelects/formSelects-v4',
    treetable: 'treetable-lay/treetable_hcloud',
    dropdown: 'dropdown/dropdown',
    notice: 'notice/notice',
    step: 'step-lay/step',
    dtree: 'dtree/dtree',
    citypicker: 'city-picker/city-picker',
    tableSelect: 'tableSelect/tableSelect'
}).use(['layer'], function () {
    var $ = layui.jquery;
    var admin = layui.admin;

    // 加载缓存的主题
    var theme = layui.data('easyweb').theme;
    if (theme) {
        layui.link('./assets/css/' + theme + '.css');
    }else{
        layui.link('./assets/css/' + 'theme-cyan' + '.css');
    }


    // 移除loading动画
    setTimeout(function () {
        $('.page-loading').addClass('layui-hide');
    }, window == top ? 500 : 300);

});