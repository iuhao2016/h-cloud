package com.hcloud.common.social.exception;

import org.springframework.security.core.AuthenticationException;

/**
 * @Auther hepangui
 * @Date 2018/12/13
 */
public class QqNotBindException extends AuthenticationException {
    public QqNotBindException(String msg) {
        super(msg);
    }

    public QqNotBindException() {
        super("qq未绑定账户");
    }
}
