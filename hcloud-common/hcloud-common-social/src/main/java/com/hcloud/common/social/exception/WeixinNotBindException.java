package com.hcloud.common.social.exception;

import org.springframework.security.core.AuthenticationException;

/**
 * @Auther hepangui
 * @Date 2018/12/13
 */
public class WeixinNotBindException extends AuthenticationException {
    public WeixinNotBindException(String msg) {
        super(msg);
    }

    public WeixinNotBindException() {
        super("微信未绑定账户");
    }
}
