package com.hcloud.common.crud.annon;

import java.lang.annotation.*;

/**
 * @Auther hepangui
 * @Date 2018/11/14
 */
@Target(ElementType.METHOD)
@Inherited
@Retention(RetentionPolicy.RUNTIME)
public @interface HCloudPreAuthorize {
    String value();
}
