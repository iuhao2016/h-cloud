package com.hcloud.system.role.repository;

import com.hcloud.system.role.entity.RoleAuthEntity;
import com.hcloud.common.crud.repository.BaseRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Auther hepangui
 * @Date 2018/11/5
 */
@Repository
public interface RoleAuthRepository extends BaseRepository<RoleAuthEntity> {
    List<RoleAuthEntity> findAllByRoleId(String roleId);

    List<RoleAuthEntity> findByRoleIdIn(List<String> roleIds);
}
