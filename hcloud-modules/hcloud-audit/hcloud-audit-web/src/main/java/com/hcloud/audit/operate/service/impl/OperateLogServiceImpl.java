package com.hcloud.audit.operate.service.impl;

import com.hcloud.audit.operate.entity.OperateLogEntity;
import com.hcloud.audit.operate.repository.OperateLogRepository;
import com.hcloud.audit.operate.service.OperateLogService;
import com.hcloud.common.crud.service.impl.BaseDataServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @Auther hepangui
 * @Date 2018/10/31
 */
@Service
public class OperateLogServiceImpl extends
        BaseDataServiceImpl<OperateLogEntity, OperateLogRepository>
        implements OperateLogService {

}
