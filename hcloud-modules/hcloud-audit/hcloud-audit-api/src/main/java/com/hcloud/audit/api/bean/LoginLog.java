package com.hcloud.audit.api.bean;

import com.hcloud.common.core.base.BaseBean;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * @Auther hepangui
 * @Date 2018/11/20
 */
@Data
@Accessors(chain = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LoginLog extends BaseBean {
    private String account;
    private String loginType;
    private String username;
    private String osName;
    private String device;
    private String browser;
    private String ipAddr;
    private Integer success;
    private String msg;
}
