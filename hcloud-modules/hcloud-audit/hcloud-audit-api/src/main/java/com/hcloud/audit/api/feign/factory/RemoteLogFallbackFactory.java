package com.hcloud.audit.api.feign.factory;

import com.hcloud.audit.api.feign.RemoteLogService;
import com.hcloud.audit.api.feign.fallback.RemoteLogFallbackImpl;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * @Auther hepangui
 * @Date 2018/11/8
 */
@Component
public class RemoteLogFallbackFactory implements FallbackFactory<RemoteLogService> {
    @Override
    public RemoteLogService create(Throwable throwable) {
        return new RemoteLogFallbackImpl(throwable);
    }
}
